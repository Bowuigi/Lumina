local Lu = require "lumina"

-- Uses currying and Lua syntactical magic to do Lumina UIs with a very nice syntax
--
-- General syntax:
-- Component [options] label contents callback
--
-- + Contents and callback may or may not be present depending on the component
-- + Options is optional
--
--[[ Small example
local ui = {
	Text "Hello world!";
	Fold "Default fold" {
		Text "Hi"
	};
	Fold {unfolded=true} "Unfolded by default" {
		Text "Hi"
	};
	Button "This is a button" (function() return "Callback!" end);
}
--]]

function Theme(t)
	t.bg = t.bg or Lu.color.default
	t.fg = t.fg or Lu.color.default

	t.text_bg = t.text_bg or Lu.color.default
	t.text_fg = t.text_fg or Lu.color.default

	t.selected_bg = t.selected_bg or Lu.color.default | Lu.color.reverse
	t.selected_fg = t.selected_fg or Lu.color.default | Lu.color.reverse

	t.title_bg = t.title_bg or Lu.color.blue
	t.title_fg = t.title_fg or Lu.color.white

	t.msg_bg = t.msg_bg or Lu.color.red
	t.msg_fg = t.msg_fg or Lu.color.white

	t.fold_bg = t.fold_bg or Lu.color.magenta
	t.fold_fg = t.fold_fg or Lu.color.white

	return t
end

function Text(label)
	return label
end

function Fold(opt_or_label)
	return function(opt_or_label)
		-- Opt
		if type(opt_or_label) == "table" then
			return function(label)
				return function(c)
					opt_or_label[1] = label
					opt_or_label[2] = c
					opt_or_label.type = Lu.fold
					return opt_or_label
				end
			end
		else -- Label
			return function(c)
				local tmp = {}
				tmp[1] = opt_or_label
				tmp[2] = c
				tmp.type = Lu.fold
				return tmp
			end
		end
	end
end

function Button(label)
	return function(callback)
		return {label, callback, type=Lu.button}
	end
end

function Toggle(opt_or_label)
	return function(opt_or_label)
		-- Opt
		if type(opt_or_label) == "table" then
			return function(label)
				return function(callback)
					opt_or_label[1] = label
					opt_or_label[2] = callback
					opt_or_label.type = Lu.toggle
					return opt_or_label
				end
			end
		else -- Label
			return function(callback)
				local tmp = {}
				tmp[1] = opt_or_label
				tmp[2] = callback
				tmp.type = Lu.toggle
				return tmp
			end
		end
	end
end

function Input(opt_or_label)
	return function(opt_or_label)
		-- Opt
		if type(opt_or_label) == "table" then
			return function(label)
				return function(callback)
					opt_or_label[1] = label
					opt_or_label[2] = callback
					opt_or_label.type = Lu.input
					return opt_or_label
				end
			end
		else -- Label
			return function(callback)
				local tmp = {}
				tmp[1] = opt_or_label
				tmp[2] = callback
				tmp.type = Lu.input
				return tmp
			end
		end
	end
end

function Radio(opt_or_label)
	return function(opt_or_label)
		-- Opt
		if type(opt_or_label) == "table" then
			return function(label)
				return function(options)
					return function (callback)
						opt_or_label[1] = label
						opt_or_label[2] = options
						opt_or_label[3] = callback
						opt_or_label.type = Lu.input
						return opt_or_label
					end
				end
			end
		else -- Label
			return function(options)
				return function(callback)
					local tmp = {}
					tmp[1] = opt_or_label
					tmp[2] = options
					tmp[3] = callback
					tmp.type = Lu.radio
					return tmp
				end
			end
		end
	end
end

