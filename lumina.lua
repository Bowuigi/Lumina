local L = require "luabox"

local lumina = {}

-- All the Luabox colors
lumina.color = {
	bold = L.BOLD,
	underline = L.UNDERLINE,
	reverse = L.REVERSE,
	default = L.DEFAULT,
	red = L.RED,
	green = L.GREEN,
	yellow = L.YELLOW,
	blue = L.BLUE,
	magenta = L.MAGENTA,
	cyan = L.CYAN,
	lighter_gray = L.LIGHTER_GRAY,
	medium_gray = L.MEDIUM_GRAY,
	gray = L.GRAY,
	light_red = L.LIGHT_RED,
	light_green = L.LIGHT_GREEN,
	light_yellow = L.LIGHT_YELLOW,
	light_blue = L.LIGHT_BLUE,
	light_magenta = L.LIGHT_MAGENTA,
	light_cyan = L.LIGHT_CYAN,
	white = L.WHITE,
	black = L.BLACK,
	darkest_gray = L.DARKEST_GRAY,
	darker_gray = L.DARKER_GRAY,
	dark_gray = L.DARK_GRAY,
	light_gray = L.LIGHT_GRAY,
	lightest_gray = L.LIGHTEST_GRAY,
	none = L.NONE,
	darkest_grey = L.DARKEST_GREY,
	darker_grey = L.DARKER_GREY,
	dark_grey = L.DARK_GREY,
	medium_grey = L.MEDIUM_GREY,
	grey = L.GREY,
	light_grey = L.LIGHT_GREY,
	lighter_grey = L.LIGHTER_GREY,
	lightest_grey = L.LIGHTEST_GREY
}

-- All the Lumina components (except non-interactive text)
lumina.type = {
	fold = "Fold",
	button = "Button",
	input = "Input",
	toggle = "Toggle",
	radio = "Radio"
}

local Default_options = {
	keybindings = {
		[L.KEY_ENTER]      = lumina.interact;
		[L.KEY_ARROW_UP]   = lumina.cursor_up;
		[L.KEY_ARROW_DOWN] = lumina.cursor_down;
		[L.KEY_CTRL_C]     = function() return lumina.quit end;
		q = function() return lumina.quit end
	},

	title = "Lumina application",

	theme = {
		bg = lumina.color.default,
		fg = lumina.color.default,
		
		text_bg = lumina.color.default,
		text_fg = lumina.color.default,

		selected_bg = lumina.color.default | lumina.color.reverse,
		selected_fg = lumina.color.default | lumina.color.reverse,

		title_bg = lumina.color.blue,
		title_fg = lumina.color.white,

		msg_bg = lumina.color.red,
		msg_fg = lumina.color.white,

		fold_bg = lumina.color.magenta,
		fold_fg = lumina.color.white
	}
}

local Scr = {
	w = 0,
	h = 0,
	cx = 0,
	scroll = 0
}

lumina.shutdown = function() L.clear_screen() L.shutdown() end
lumina.quit = false

local function Switch(expr)
	return function (cases)
		local run = cases[expr] or cases.default

		if run then
			return run()
		end
	end
end

function lumina.interact()
	-- Get current element
	-- Interact with it, call callback as needed
end

function lumina.cursor_up()
	if Scr.cx == 0 then return end
	Scr.cx = Scr.cx - 1
	Scr.scroll = Scr.scroll - 1
end

function lumina.cursor_down()
	Scr.cx = Scr.cx + 1
	Scr.scroll = Scr.scroll + 1
end

function lumina.init()
	if not L.init() then
		io.stderr:write("[Lumina] Fatal error: Failed to init Luabox\n")
		os.exit(1)
	end

	Scr.w = L.width()
	Scr.h = L.height()

	L.clear_screen()
	L.hide_cursor()
end

function lumina.process(keybindings)
	local ev = {}

	local t = L.poll_event(ev)

	continue = Switch (t) {
		[L.EVENT_KEY] = keybindings[ev.key] or keybindings[ev.ch] or function() end;
		[L.EVENT_RESIZE] = function()
			L.resize()
			Scr.w = ev.w
			Scr.h = ev.h
		end;
	}

	if continue == nil then continue = true end
	return continue
end

function lumina.render(title, ui, theme)
	-- TODO: Account for multiline components (fold, multiline text, radios, etc)
	for draw=Scr.scroll, Scr.h+Scr.scroll do
		if type(ui[draw]) == "string" then
			L.string(1, draw-Scr.scroll, theme.text_fg, theme.text_bg, ui[draw])
		elseif type(ui[draw]) == "table" then
			L.string(1, draw-Scr.scroll, theme.text_fg, theme.text_bg, "Component ["..ui[draw].type.."] Label ["..ui[draw][1].."]")
		end
	end

	L.render()
end

function lumina.run(options)
	local ui = options.ui or error("An UI is needed to run Lumina")
	local title = options.title or Default_options.title
	local keybindings = options.keybindings or Default_options.keybindings
	local theme = options.theme or Default_options.theme

	lumina.init()

	local running = true

	while running do
		lumina.render(title, ui, theme)
		running = lumina.process(keybindings)
	end

	lumina.shutdown()
end

setmetatable(lumina, {__call = function (self, options) lumina.run(options) end})
return lumina

