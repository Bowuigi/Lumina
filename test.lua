local Lu = require "lumina"

local ui = {
	-- Components can be strings or tables, strings are non-interactive text
	"Normal text, cannot interact with it";

	-- Folds are unfolded by default, the 'unfolded' component setting changes this
	{"Fold test", {
		"Oh hello, you can fold and unfold this with enter";
		{"Fold inside a fold", {
			"Hello lol"
		}, type=Lu.type.fold};
	}, type=Lu.type.fold, unfolded=true};

	-- Callback return values are usually errors
	{"Button", function() return "Error! Do not press this button!" end, type=Lu.type.button};

	{"Input", function(value) option=value end, type=Lu.type.input, value="Type here..."};

	{"Toggle",
		function(value)
			-- return error_condition, reset_to_previous_value
			return "Can not set toggle", true
		end,
		type=Lu.type.toggle, on=false};

	{"Radio", {
		"Option A",
		"Option B",
		"Option C"
	}, function(value) return "Set to "..value.."!" end, type=Lu.type.radio};
}

Lu {
	ui = ui,
	title = "Lumina test"
}

